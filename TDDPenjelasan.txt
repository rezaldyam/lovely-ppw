from django.test import TestCase, Client
from .views import matkul_page, form_matkul

 

class UnitTest (TestCase):
=======================================================================

# Mengecek sudah ada app bernama story_6 atau belum
class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story6Config.name, "story_6")'

=======================================================================

#Mengecek function berhasil dipanggil atau tidak
class TestFunc(TestCase):
	def test_kegiatan_view_func(self):
		found = resolve('/story6/')
		self.assertEqual(found.func, kegiatan_view)
	
	def test_kegiatan_tambah_func(self):
		found = resolve('/story6/tambah/')
		self.assertEqual(found.func, Kegiatan_tambah)

	def test_views3_func(self):
		activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		getid = activity.id
		found = resolve('/story6/tambahpeserta/' +str(getid)+ '/')
		self.assertEqual(found.func, Peserta_tambah)
		
=======================================================================

#REKOMEN PAK ADIN
#TEST Apakah terdapat atribut tersebut dalam tampilan halaman
def test_view_in_template(self):
    response = Client().get('/hasil/')
    self.assertTemplateUsed(response.content.decode('utf8')

    self.assertIn('Buku Tamu', isi_html_kembalian)
    self.assertIn('Jumlah Tamu', isi_html_kembalian)
    self.assertIn('NamaTamu', isi_html_kembalian)
    self.assertIn('Jumlah Tamu', isi_html_kembalian)
    self.assertIn("<a href='/formulir'/>Link untuk Kembali ke halaman</a>" , isi_html_kembalian)
================================================================

#REKOMEN PAK ADIN
# Test untuk mengetahui apakah URL telah tersedia
def test_url_is_exist(self):
    response = Client().get('/jadwalmatkul/')
    self.assertEqual(200, response.status_code)
def test_event3_url_is_exist(self):
    activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
    getid = activity.id
    response = Client().get('/story6/tambahpeserta/' +str(getid)+ '/')
    self.assertEqual(response.status_code, 200)
def test_event_using_template(self):
    response = Client().get('/story6/')
    self.assertTemplateUsed(response, 'kegiatan_view.html')
def test_event2_using_template(self):

=======================================================================
#Test untuk menguji function daftar
class TestDaftar(TestCase):
	def setUp(self):
		kegiatan = Kegiatan(namaKegiatan="PPW")
		kegiatan.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

=========================================================================

# Test untuk mengetahui apakah pada kolom yang disediakan telah terisi dengan panjang kata/kalimat yang ditentukan
def test_matkul_page_is_written(self):
    self.assertIsNotNone(matkul_page)
    self.assertTrue(len(matkul_page) >= 40

=======================================================================

# Test untuk mengetahui apakah form yang selesai diisi
def test_matkul_page_is_completed(self):
    request = HttpRequest()
    response = index(request)
    html_response = response.content.code('utf8')
    self.assertIn(matkul_page, html_response)

=======================================================================

# Test untuk mengetahui apakah isi HTML sudah sesuai
def test_content_html(self):
    self.assertIn('Nama Mata Kuliah', form_matkul),
    self.assertIn('Deskripsi', form_matkul),
    self.assertIn('Nama Dosen', form_matkul),
    self.assertIn('Ruang', form_matkul),
    self.assertIn('Jumlah SKS', form_matkul),
    self.assertIn('Semester Tahun', form_matkul)

=======================================================================

#Test untuk melihat apakah hasil dari form terlihat oleh user
def test_list_matkul(self):
    arg = {
        'matkul': 'SDA',
        'deskripsi': 'Mata kuliah pemograman',
        'nama_dosen': 'Budi',
        'ruang': '5',
        'jumlah_sks:'4',
        'semester_tahun' : 'Gasal 2020/2021',

    }

    response = Client.post('/listmatkul/', arg)
    isi_html_matkul = response.content.decode('utf8)
    self.assertIn('SDA', isi_html_matkul)
    self.assertIn('Mata kuliah pemograman', isi_html_matkul)
    self.assertIn('Budi', isi_html_matkul)
    self.assertIn('5', isi_html_matkul)
    self.assertIn('4', isi_html_matkul)
    self.assertIn('Gasal 2020/2021', isi_html_matkul)

============================================================================

#REKOMEN PAK ADIN
#Test model
class TestModels(TestCase):
	def test_model_can_create(self):
		new_activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		counting_all_available_todo = Kegiatan.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)

	def test_model2_can_create(self):
		activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		peserta = Peserta.objects.create(namaPeserta="Zaldy", daftarKegiatan=activity)
		counting_all_available_todo = Peserta.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
	
	def test_model_can_print(self):
		kegiatan = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		self.assertEqual(kegiatan.__str__(), "Startup Academy")
	
	def test_model2_can_print(self):
		activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		peserta = Peserta.objects.create(namaPeserta="Zaldy",daftarKegiatan=activity)
		self.assertEqual(peserta.__str__(), "Zaldy")

