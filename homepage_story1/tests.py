from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client
from django.urls import resolve
from .apps import HomepageStory1Config
from homepage.views import index_story1
from homepage.views import index


# Create your tests here.

class TestApp(TestCase):
    def test_app_homepage1(self):
        self.assertEqual(HomepageStory1Config.name, "homepage_story1")


class TestFunc(TestCase):
    def test_index_homepage1(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_index_story1_homepage1(self):
        found = resolve('/story1/')
        self.assertEqual(found.func, index_story1)


class TestRouting(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        
    def test_indstory1_url_is_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

        



    


    

    