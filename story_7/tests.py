from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import Story7Config
from .views import index


# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story7Config.name, "story_7")


class TestRouting(TestCase):
	def test_index_url_is_exist(self):
		response = Client().get('/story7/')
		self.assertEqual(response.status_code, 200)


class TestFunc(TestCase):
	def test_kegiatan_view_func(self):
		found = resolve('/story7/')
		self.assertEqual(found.func, index)

