from django.shortcuts import render, redirect, reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
# Create your views here.
def index(request):
    return render(request, 'index_9.html')
def user_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        #menggunakan authenticate() untuk memverifikasi kredensial yang ada
        user = authenticate(request, username=username, password=password)
        #backend mengautentikasi kredensial
        if user != None:
            login(request, user)
            return redirect('story_9:index')
        else:
        #backend tidak mengautentikasi kredensial
            context = {
                'p':'Akun anda belum terdaftar :('
            }
            return render(request, 'login.html', context)
    return render(request, 'login.html')
def user_logout(request):
    #menghapus session dengan cara memaksa hibernate
    logout(request)
    return redirect('story_9:index')
def register(request):
    UserForm = UserCreationForm()
    context = {
        'form': UserForm,
    }
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        #validator
        if form.is_valid():
            form.save()
            return redirect('story_9:user_login')
        else :
            context.update({'p':'Input salah, lihat ketentuan dibawah...' })
    return render(request,'signup.html', context)