from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import Story9Config
from .views import index
from .views import index, user_login, user_logout, register
from django.contrib.auth.models import User
from requests.api import request
from django.contrib.auth.forms import UserCreationForm

# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story9Config.name, "story_9")

class TestRouting(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_2_exist(self):
            response = Client().get('/story9/login/')
            self.assertEqual(response.status_code, 200)
        
    def test_index_url_3_exist(self):
            response = Client().get('/story9/register/')
            self.assertEqual(response.status_code, 200)

class TestFunc(TestCase):
    def test_fungsiurl_view_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)
class TestPageContent(TestCase):
    def test_index_in_template(self):
        response = Client().get('/story9/')
        konten_html = response.content.decode('utf8')
        self.assertIn('Story 9!', konten_html)
        self.assertIn('Sign', konten_html)
        self.assertIn('Login', konten_html)
class TestForm(TestCase):
	def test_form_invalid(self):
		form_kegiatan = UserCreationForm({})
		self.assertFalse(form_kegiatan.is_valid())
		form_orang = UserCreationForm(data={})
		self.assertFalse(form_orang.is_valid())
class TestLogout(TestCase):
	def test_logout_valid(self):
            new_user = User.objects.create_user("dsadsada@J88", "rezaldyam@gmail.comm", "dsadas23m*@#")
            counting_all_available_todo = User.objects.all().count()
            self.assertEqual(counting_all_available_todo, 1)
class TestIndex(TestCase):
	def test_index_valid(self):
            new_user = User.objects.create_user("dsadsada@J88", "rezaldyam@gmail.comm", "dsadas23m*@#")
            counting_all_available_todo = User.objects.all().count()
            self.assertEqual(counting_all_available_todo, 1)
            queryset = User.objects.all()
            if (new_user!= None):
                response = Client().get('/story9/')
                self.assertEqual(response.status_code, 200)


