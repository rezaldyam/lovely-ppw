from django.urls import path
from . import views

app_name = 'story_9'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/',views.user_login, name='user_login'),
    path('logout/',views.user_logout, name='user_logout'),
    path('register/',views.register, name='register'),
    
]
