from django.shortcuts import render, get_object_or_404, render, redirect
from .models import Kegiatan, Peserta
from .forms import KegiatanForms, PesertaForms

# from .forms import MataKuliahForms #GANTIIIII
from django.contrib import messages
# Create your views here.

def kegiatan_view(request):
    queryset = Kegiatan.objects.all()
    kumpulanPeserta = Peserta.objects.all()
    return render(request, "kegiatan_view.html", {"kumpulanPeserta":kumpulanPeserta, "semuaKegiatan":queryset})


def Kegiatan_tambah(request):
    
    form = KegiatanForms(request.POST or None)
    if form.is_valid(): #validator
        form.save()
    context = {
        'form' : form
    }
    return render(request, "kegiatan_form.html", context)

def Peserta_tambah(request, id=0):
    if request.method == "POST":
        form = PesertaForms(request.POST or None)
        if form.is_valid():
            pendaftarValid = Peserta(daftarKegiatan=Kegiatan.objects.get(pk=id), namaPeserta=form.data['namaPeserta'])
            pendaftarValid.save()
        return redirect('/story6/')
    form = PesertaForms()
    return render(request, "peserta_form.html", {'peserta': form})
