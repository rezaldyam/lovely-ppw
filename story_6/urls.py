from django.urls import path
from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.kegiatan_view, name='kegiatan_view'),
    path('tambah/', views.Kegiatan_tambah, name='Kegiatan_tambah'),
    path('tambahpeserta/<int:id>/', views.Peserta_tambah, name='Peserta_tambah'),
    
]

