from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=100)
    
    def __str__(self):
        return self.namaKegiatan

class Peserta(models.Model):
    namaPeserta = models.CharField(max_length=100)
    daftarKegiatan = models.ForeignKey(Kegiatan, null=True, on_delete= models.CASCADE)

    def __str__(self):
        return self.namaPeserta

