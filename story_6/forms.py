from django import forms
from .models import Kegiatan, Peserta


class KegiatanForms(forms.ModelForm):
    namaKegiatan = forms.CharField(label='Nama Kegiatan', 
                    widget=forms.TextInput(attrs={"placeholder": "Kegiatan"}), )
   

    class Meta:
        model = Kegiatan
        fields = [
            'namaKegiatan',
            
        ]
    
class PesertaForms(forms.ModelForm):
    namaPeserta = forms.CharField(label='Nama Anda', 
                    widget=forms.TextInput(attrs={"placeholder": "Masukkan Nama"}), )
   

    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
            
        ]
    
