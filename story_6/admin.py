from django.contrib import admin
from .views import Kegiatan, Peserta
# Register your models here.


admin.site.register(Kegiatan)
admin.site.register(Peserta)

