from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import Story6Config
from .views import kegiatan_view, Kegiatan_tambah, Peserta_tambah
from .models import Kegiatan, Peserta
from story_6.forms import KegiatanForms, PesertaForms





# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story6Config.name, "story_6")



class TestDaftar(TestCase):
	def setUp(self):
		kegiatan = Kegiatan(namaKegiatan="PPW")
		kegiatan.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)


class TestRouting(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get('/story6/')
		self.assertEqual(response.status_code, 200)
	def test_event2_url_is_exist(self):
		response = Client().get('/story6/tambah/')
		self.assertEqual(response.status_code, 200)
	def test_event3_url_is_exist(self):
		activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		getid = activity.id
		response = Client().get('/story6/tambahpeserta/' +str(getid)+ '/')
		self.assertEqual(response.status_code, 200)
	def test_event_using_template(self):
		response = Client().get('/story6/')
		self.assertTemplateUsed(response, 'kegiatan_view.html')
	def test_event2_using_template(self):
		response = Client().get('/story6/tambah/')
		self.assertTemplateUsed(response, 'kegiatan_form.html')


	
class TestModels(TestCase):
	def test_model_can_create(self):
		new_activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		counting_all_available_todo = Kegiatan.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)

	def test_model2_can_create(self):
		activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		peserta = Peserta.objects.create(namaPeserta="Zaldy", daftarKegiatan=activity)
		counting_all_available_todo = Peserta.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
	
	def test_model_can_print(self):
		kegiatan = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		self.assertEqual(kegiatan.__str__(), "Startup Academy")
	
	def test_model2_can_print(self):
		activity = Kegiatan.objects.create(namaKegiatan="Startup Academy")
		peserta = Peserta.objects.create(namaPeserta="Zaldy",daftarKegiatan=activity)
		self.assertEqual(peserta.__str__(), "Zaldy")


class TestForm(TestCase):

	def test_form_is_valid(self):
		form_kegiatan = KegiatanForms(data={
			"namaKegiatan": "SUA",
		})
		self.assertTrue(form_kegiatan.is_valid())
		form_orang = PesertaForms(data={
			'namaPeserta': "Joko Williwonka"
		})
		self.assertTrue(form_orang.is_valid())

	def test_form_invalid(self):
		form_kegiatan = KegiatanForms(data={})
		self.assertFalse(form_kegiatan.is_valid())
		form_orang = KegiatanForms(data={})
		self.assertFalse(form_orang.is_valid())


