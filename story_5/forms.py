from django import forms
from .models import MataKuliah

class MataKuliahForms(forms.ModelForm):
    nama = forms.CharField(label='Nama Matkul', 
                    widget=forms.TextInput(attrs={"placeholder": "Mata Kuliah"}), )
    dosen = forms.CharField(label='Nama Dosen', 
                    widget=forms.TextInput(attrs={"placeholder": "Nama Dosen"}))
    ruangkelas = forms.CharField(label='Ruang Kelas', 
                    widget=forms.TextInput(attrs={"placeholder": "Ruang Kelas"}))

    
    class Meta:
        model = MataKuliah
        fields = [
            'nama',
            'dosen',
            'ruangkelas',
            'semestertahun',
            'jumlahsks',
            'deskripsi'
        ]
    
