from django.test import TestCase, Client
from django.urls import resolve
from .apps import Story5Config
from .views import index_story5, MataKuliah_detail_view, MataKuliah_hapus_view, MataKuliah_list_view, MataKuliah_tambah_view
from .models import MataKuliah


# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story5Config.name, "story_5")


class TestFunc(TestCase):
	def test_kegiatan_view_func(self):
		found = resolve('/story5/')
		self.assertEqual(found.func, index_story5)

	def test_kegiatan_tambah_func(self):
		found = resolve('/story5/list/')
		self.assertEqual(found.func, MataKuliah_list_view)

	def test_views3_func(self):
		found = resolve('/story5/tambah/')
		self.assertEqual(found.func, MataKuliah_tambah_view)


class TestRouting(TestCase):
   
	def test_event_url_is_exist(self):
		response = Client().get('/story5/')
		self.assertEqual(response.status_code, 200)

	def test_event2_url_is_exist(self):
		response = Client().get('/story5/tambah/')
		self.assertEqual(response.status_code, 200)

	def test_matkul_url_is_exist(self):
		activity = MataKuliah.objects.create(nama="SDA", dosen="zaldy", jumlahsks=4,deskripsi='dsadasd', ruangkelas='aaaaa', semestertahun=1)
		getid = activity.id
		response = Client().get('/story5/' + str(getid) +'/hapus/')
		self.assertEqual(response.status_code, 200)

	def test_event_using_template(self):
		response = Client().get('/story5/')
		self.assertTemplateUsed(response, 'index_story5.html')

	def test_event2_using_template(self):
		response = Client().get('/story5/tambah/')
		self.assertTemplateUsed(response, 'matkul_tambah.html')
        
	
class TestModels(TestCase):
	def test_model_can_create(self):
		new_matkul = MataKuliah.objects.create(nama="SDA", dosen="zaldy", jumlahsks=4,deskripsi='dsadasd', ruangkelas='aaaaa', semestertahun=1)
		counting_all_available_todo = MataKuliah.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)


class TestDaftar(TestCase):
	def setUp(self):
		activity = MataKuliah.objects.create(nama="SDA", dosen="zaldy", jumlahsks=4,deskripsi='dsadasd', ruangkelas='aaaaa', semestertahun=1)
		activity.save()
		self.assertEqual(MataKuliah.objects.all().count(), 1)
