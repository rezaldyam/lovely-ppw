from django.shortcuts import get_object_or_404, render, redirect

from .models import MataKuliah
from .forms import MataKuliahForms
from django.contrib import messages
# Create your views here.

def index_story5(request):
    return render(request, 'index_story5.html')

def MataKuliah_tambah_view(request):
    form = MataKuliahForms(request.POST or None)
    if form.is_valid(): #validator
        form.save()
    
    context = {
        'form' : form
    }
    return render(request, "matkul_tambah.html", context)


def MataKuliah_hapus_view(request, id):
    obj = get_object_or_404(MataKuliah, id=id)
    if request.method == "POST":
        # Mengkonfirmasi penghapusan
        obj.delete()
    context = {
        'Object' : obj
    }
    return render(request, "matkul_hapus.html", context)


def MataKuliah_list_view(request):
    queryset = MataKuliah.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "matkul_list.html", context)


def MataKuliah_detail_view(request, id):
    obj = get_object_or_404(MataKuliah, id=id)
    context = {
        'Object' : obj
    }
    return render(request, 'matkul_detail.html', context)

# Input :
# - Nama Mata Kuliah
# - Dosen Pengajar
# - Jumlah SKS (number integer)
# - Deskripsi
# - Ruang
# - Semester Tahiun

# Fitur :
# - tambah
# - hapus
# - ketika sudah ada maka terupdate, jika belum maka akan tertambah
# - detail mata kuliah