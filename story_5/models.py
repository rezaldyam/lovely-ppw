from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse

# Create your models here.
class MataKuliah(models.Model):
    nama = models.CharField(max_length=120)
    dosen = models.CharField(max_length=120)
    jumlahsks = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(6)])
    deskripsi = models.TextField(blank=True, null=True)
    ruangkelas = models.CharField(max_length=120, default='DEFAULT VALUE')
    CHOICES = (('a','Gasal 2019/2020'),
               ('b','Genap 2018/2019'),
               ('c','Gasal 2018/2019'),
               ('d','Genap 2017/2018'),
               ('e','Gasal 2018/2018'),
               ('f','Genap 2016/2017'),
               ('g','Gasal 2016/2017'),)
    semestertahun = models.CharField(max_length = 20, choices = CHOICES, default = '1',)

    def __str__(self):
            return self.nama



    