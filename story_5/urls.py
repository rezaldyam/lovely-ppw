from django.urls import path
from . import views
from django.test import Client

app_name = 'story_5'

urlpatterns = [
    path('', views.index_story5, name='index'),
    path('list/', views.MataKuliah_list_view, name='MataKuliah_list_view'),
    path('<int:id>/hapus/', views.MataKuliah_hapus_view, name='MataKuliah_hapus_view'),
    path('tambah/', views.MataKuliah_tambah_view, name='MataKuliah_tambah_view'),
    path('<int:id>/detail/', views.MataKuliah_detail_view, name='MataKuliah_detail_view')
]

