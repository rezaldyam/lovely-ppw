from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from homepage.views import index
from story_5.views import index_story5
from homepage_story1.views import index_story1
from story_6.views import kegiatan_view

# Create your tests here.

class TestFunc(TestCase):
	def test_proj_2_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_prj_3_func(self):
		found = resolve('/story1/')
		self.assertEqual(found.func, index_story1)


	def test_prj_3_func(self):
		found = resolve('/story5/')
		self.assertEqual(found.func, index_story5)


	def test_prj_3_func(self):
		found = resolve('/story6/')
		self.assertEqual(found.func, kegiatan_view)
	

class TestRouting(TestCase):
	def test_prok_url_is_exist(self):
		response = Client().get('/story6/')
		self.assertEqual(response.status_code, 200)
	def test_proj3_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_event2_using_template(self):
		response = Client().get('/story1/')
		self.assertEqual(response.status_code, 200)
