from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('film/', views.film, name='film'),
    path('story1/',views.index_story1, name='index_story1')
]