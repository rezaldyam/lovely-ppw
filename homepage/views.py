from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'index.html')

def film(request):
    return render(request, 'film_rec.html')

def index_story1(request):
    return render(request, 'index_story1.html')