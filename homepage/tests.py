from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client
from django.urls import resolve
from .apps import HomepageConfig
from .views import index, film, index_story1

class TestApp(TestCase):
    def test_app_homepage(self):
        self.assertEqual(HomepageConfig.name, "homepage")

class TestFunc(TestCase):
    def test_index_homepage(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_story1_homepage(self):
        found = resolve('/story1/')
        self.assertEqual(found.func, index_story1)

    def test_film_homepage(self):
        found = resolve('/film/')
        self.assertEqual(found.func, film)
    

class TestRouting(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_film_url_is_exist(self):
        response = Client().get('/film/')
        self.assertEqual(response.status_code, 200)

    def test_story1_url_is_exist(self):
        response = Client().get('/story1/')
        self.assertEqual(response.status_code, 200)

    def test_index_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_film_using_template(self):
        response = Client().get('/film/')
        self.assertTemplateUsed(response, 'film_rec.html')

    


    

    