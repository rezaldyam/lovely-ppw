from django.urls import path
from . import views

app_name = 'story_8'

urlpatterns = [
    path('', views.fungsi_suatu_url, name='fungsi_suatu_url'),
    path('data/',views.fungsi_data, name='fungsi_data'),
]

