from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import requests


def fungsi_suatu_url(request):
    response = {}
    return render(request,'index_8.html', response)


def fungsi_data(request):
    arg = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + arg
    req = requests.get(url)
    data = json.loads(req.content)
    return JsonResponse(data, safe=False)
