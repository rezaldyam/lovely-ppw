from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import Story8Config
from .views import fungsi_suatu_url


# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story8Config.name, "story_8")


class TestRouting(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_add_url_is_exist(self):
        response = Client().get('/story8/data/?q=book')
        self.assertEqual(response.status_code, 200)

class TestFunc(TestCase):
    def test_fungsiurl_view_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, fungsi_suatu_url)

    def test_fungsidata_is_exist(self):
        response = Client().get('/story8/data/?q=gojek')
        text = response.content.decode('utf8')
        self.assertIn("Digital", text)


class TestPageContent(TestCase):
    def test_index_in_template(self):
        response = Client().get('/story8/')
        konten_html = response.content.decode('utf8')
        self.assertIn('Story 8', konten_html)
        self.assertIn('Cari Buku', konten_html)
        self.assertIn('Cover', konten_html)
        self.assertIn('Judul', konten_html)
        self.assertIn('Penulis', konten_html)
        self.assertIn('Terbit', konten_html)
        